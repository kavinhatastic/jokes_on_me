package com.udacity.gradle.builditbigger;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static org.hamcrest.Matchers.not;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import android.support.test.espresso.core.deps.guava.util.concurrent.Service;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import javax.security.auth.callback.Callback;

@RunWith(AndroidJUnit4.class)
public class AsyncTaskTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new
            ActivityTestRule<>(MainActivity.class);

    @Test
    public void AsyncTaskNonEmpty() {
        onView(withId(R.id.androidJoke_textview))
                .check(matches(not(withText(""))));
    }


    //Sample code to test AsyncTask
    /*public void testSomething(){
        final CountDownLatch signal = new CountDownLatch(1);
        Service.doSomething(new Callback() {

            @Override
            public void onResponse(){
                // test response data
                // assertEquals(..
                // assertTrue(..

                // etc
                signal.countDown();// notify the count down latch
            }

        });
        signal.await();// wait for callback
    }*/
}
